package com.lsvirus.waft;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.swing.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 1/17/14
 * Time: 4:13 PM
 */
public class Main extends Application {
    public static final Map<String, String> COOKIE = new HashMap<>();
    public static final Executor            exec   = Executors.newCachedThreadPool();

    private TextField     usernameField;
    private TextField     institutionField;
    private PasswordField passwordField;

    public static void main(String[] args) {
        Application.launch();
    }

    @Override
    public void start(final Stage stage) throws Exception {
        final VBox vBox = new VBox(5);

        vBox.setAlignment(Pos.CENTER);
        vBox.setPadding(new Insets(10, 10, 10, 10));

        EventHandler<ActionEvent> loginEvent = new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent actionEvent) {
                exec.execute(new Runnable() {
                    @Override
                    public void run() {
                        final Node buttonNode = vBox.getChildren().get(vBox.getChildren().size() - 1);

                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                buttonNode.setDisable(true);
                            }
                        });

                        try {
                            if (login())
                                Platform.runLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            stage.setScene(new ShowAssignments(
                                                    usernameField.getText(),
                                                    institutionField.getText()).getScene());
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });

                            else
                                JOptionPane.showMessageDialog(null, "Incorrect credentials");

                        } catch (IOException e) {
                            JOptionPane.showMessageDialog(null, "Error connecting to WebAssign");
                            e.printStackTrace();
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e.getMessage());
                            e.printStackTrace();
                        }

                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                buttonNode.setDisable(false);
                            }
                        });
                    }
                });
            }
        };

        vBox.getChildren().addAll(
                usernameField = TextFieldBuilder.create().promptText("Username").onAction(loginEvent).build(),
                passwordField = PasswordFieldBuilder.create().promptText("Password").onAction(loginEvent).build(),
                institutionField = TextFieldBuilder.create().text("parkview.ga").onAction(loginEvent).build(),

                ButtonBuilder.create().text("Login").onAction(loginEvent).onAction(loginEvent).build());

        stage.setTitle("WA.FT");
        stage.setScene(new Scene(vBox));
        stage.show();
    }

    private boolean login() throws IOException {
        Connection.Response res = Jsoup.connect("https://www.webassign.net/login.html")
                .data("WebAssignUsername", usernameField.getText())
                .data("WebAssignInstitution", institutionField.getText())
                .data("WebAssignPassword", passwordField.getText())
                .data("Login.x", "21")
                .data("Login.y", "5")
                .method(Connection.Method.POST)
                .execute();

        String userpassKey = "UserPass";
        String cookie = res.cookie(userpassKey);

        if (cookie != null) {
            COOKIE.put(userpassKey, cookie);
            return true;
        } else
            return false;
    }

    public static Document goTo(String url) throws IOException {
        return Jsoup.connect(url)
                .cookies(Main.COOKIE)
                .execute().parse();
    }
}
