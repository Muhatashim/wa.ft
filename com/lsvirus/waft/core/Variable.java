package com.lsvirus.waft.core;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 1/18/14
 * Time: 10:37 AM
 */
public class Variable<V> {
    private final V var;

    public Variable(V v) {
        var = v;
    }

    public <T> T get() {
        return (T) var;
    }

    public double getDouble() {
        return (Double) var;
    }
}
