package com.lsvirus.waft.core;

import com.lsvirus.waft.Main;
import com.lsvirus.waft.core.solverimpls.hardcode.Part1Mirrors;
import com.lsvirus.waft.core.solverimpls.hardcode.Part2RefractionAndLenses;
import com.lsvirus.waft.core.solverimpls.hardcode.ThermodynamicsB;
import com.lsvirus.waft.core.solverimpls.script.Script;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.script.ScriptException;
import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 1/17/14
 * Time: 6:59 PM
 */
public class AssignmentProcessor implements Runnable {

    private String dep;
    private Solver solver;

    public static final Solver[] SOLVERS = loadSolvers();

    private static Solver[] loadSolvers() {
        List<Solver> solvers = new ArrayList<>();

        File scriptsFolder = new File("./scripts");
        for (File file : scriptsFolder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                System.out.println(name);
                return name.endsWith(".json");
            }
        }))
            try {
                solvers.add(new Script(file));

                System.out.println("LOADED " + file.getName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        solvers.addAll(Arrays.asList(
                new ThermodynamicsB(),
                new Part1Mirrors(),
                new Part2RefractionAndLenses()
        ));

        return solvers.toArray(new Solver[solvers.size()]);
    }

    public AssignmentProcessor(String dep, String assignmentName) throws Exception {
        this.dep = dep;

        for (Solver solverC : SOLVERS)
            if (solverC.getName().equals(assignmentName)) {
                solver = solverC;
                return;
            }

        throw new Exception("A solver for this assignment has not been coded yet!");
    }

    @Override
    public void run() {
        try {
            Document document = Main.goTo(String.format("http://www.webassign.net/web/Student/Assignment-Responses/last?dep=%s", dep));

            Elements questions = document.select("[class=standard qContent container] > *");
            for (int i = 0; i < questions.size(); i++) {
                Element element = questions.get(i);
                Elements select = element.select("[color=red]");

                HashMap<Integer, Variable<?>> variables = new HashMap<>();
                int variableId = 1;

                for (Element e : select) {
                    String text = e.text();

                    if (!text.isEmpty()) {
                        if (text.matches("-?\\.?\\d+\\.?\\d+ 10-?\\d+")) { //scientific
                            variables.put(variableId, new Variable<>(parseScientific(text)));
                        } else if (text.matches("-?\\.?\\d+\\.?\\d*")) {   //number
                            variables.put(variableId, new Variable<>(Double.parseDouble(text)));
                        } else {                                           //plaintext
                            variables.put(variableId, new Variable<>(text));
                        }


                        variableId++;
                    }
                }

                int relativeQ = i + 1;
                try {
                    printAnswer(relativeQ, solver.solve(relativeQ, variables));
                } catch (ScriptException e) {
                    printAnswer(relativeQ, new String[]{"Error trying to solve this problem"});
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error, retry later.");
        }
    }

    private void printAnswer(int relativeQ, String[] solve) {
        System.out.print(relativeQ + ". ");
        char current = 'a';

        if (solve == null) {
            System.out.println("Cannot solve");
            return;
        }

        if (solve.length > 1) {
            System.out.println();
            for (int i = 0; i < solve.length; i++) {
                if (solve[i] != null)
                    System.out.println("\t" + current + ". " + solve[i]);
                else
                    System.out.println("\t" + current + ". " + "Cannot solve");

                current++;
            }
        } else
            System.out.println(solve[0]);
    }

    private static double parseScientific(String text) {
        String[] split = text.split("-?\\.?\\d+\\.?\\d*-?\\d+");

        double coef = Double.parseDouble(split[0]);
        double pow = Double.parseDouble(split[1].substring(2));
        return coef * Math.pow(10, pow);
    }

}
