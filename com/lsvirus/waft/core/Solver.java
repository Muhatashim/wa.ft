package com.lsvirus.waft.core;

import javax.script.ScriptException;
import java.io.FileNotFoundException;
import java.util.HashMap;

/**
 * Created by Muhatashim on 1/17/14.
 */
public abstract class Solver {

    public abstract String getName();

    public abstract String[] solve(int questionNum, HashMap<Integer, Variable<?>> variables) throws FileNotFoundException, ScriptException;

    protected static double deg(double num) {
        return Math.toDegrees(num);
    }

    protected static double rad(double num) {
        return Math.toRadians(num);
    }

    protected static double pow(int to) {
        return Math.pow(10, to);
    }

    protected static String format(double num) {
        if (num == 0)
            return "0";

        final double d = Math.ceil(Math.log10(num < 0 ? -num : num));
        final int power = randomSigFigs() - (int) d;

        final double magnitude = pow(power);
        final long shifted = Math.round(num * magnitude);
        return String.valueOf(shifted / magnitude);
    }

    protected String[] formatDoubleToStringArr(double... n) {
        String[] arr = new String[n.length];

        for (int i = 0; i < arr.length; i++)
            arr[i] = format(n[i]);
        return arr;
    }

    protected static int randomSigFigs() {
        return (int) (Math.random() * 2 + 4);
    }
}
