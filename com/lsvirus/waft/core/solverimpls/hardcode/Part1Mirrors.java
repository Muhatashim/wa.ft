package com.lsvirus.waft.core.solverimpls.hardcode;

import com.lsvirus.waft.core.Solver;
import com.lsvirus.waft.core.Variable;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 2/22/14
 * Time: 12:22 PM
 */
public class Part1Mirrors extends Solver {

    @Override
    public String getName() {
        return "Chapter 25 and Chapter 26, part 1, Mirrors";
    }

    @Override
    public String[] solve(int questionNum, HashMap<Integer, Variable<?>> variables) {
        switch (questionNum) {
            case 1: {
                return formatDoubleToStringArr(variables.get(1).getDouble());
            }
            case 2: {
                return new String[]{"1.283", "186.67"};
            }
            case 3: {
                double f = variables.get(1).getDouble() * pow(14);

                double ans = .02 / (300000000 / f);
                return formatDoubleToStringArr(ans);
            }
            case 4: {
                double dist = variables.get(1).getDouble() * pow(-2);

                double ans = 3E8 / (dist * 4);
                return formatDoubleToStringArr(ans);
            }
            case 5: {
                double angle = variables.get(1).get();

                double ans = 120 - angle;
                return formatDoubleToStringArr(ans);
            }
            case 6: {
                double ans = 0;

                return formatDoubleToStringArr(ans);
            }
            case 7: {
                double ans = 0;

                return formatDoubleToStringArr(ans);
            }
            case 8: {
                double ans = 0;

                return formatDoubleToStringArr(ans);
            }
            case 9: {
                double ans = 0;

                return formatDoubleToStringArr(ans);
            }
            case 10: {
                double ans = 0;

                return formatDoubleToStringArr(ans);
            }
            case 11: {
                double ans = 0;

                return formatDoubleToStringArr(ans);
            }
            case 12: {
                double ans = 0;

                return formatDoubleToStringArr(ans);
            }
            case 13: {
                double ans = 0;

                return formatDoubleToStringArr(ans);
            }
            case 14: {
                double ans = 0;

                return formatDoubleToStringArr(ans);
            }
            case 15: {
                double ans = 0;

                return formatDoubleToStringArr(ans);
            }
            case 16: {
                double ans = 0;

                return formatDoubleToStringArr(ans);
            }
        }

        return null;
    }
}
