package com.lsvirus.waft.core.solverimpls.hardcode;

import com.lsvirus.waft.core.Solver;
import com.lsvirus.waft.core.Variable;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 3/2/14
 * Time: 2:35 PM
 */
public class Part2RefractionAndLenses extends Solver {

    @Override
    public String getName() {
        return "Chapter 26, part 2 Refraction and Lenses";
    }

    @Override
    public String[] solve(int questionNum, HashMap<Integer, Variable<?>> variables) {
        switch (questionNum) {
            case 1: {
                double distance = variables.get(1).getDouble();
                double time = variables.get(2).getDouble() * pow(-9);

                double ans = 3E8 / (distance / time);
                return formatDoubleToStringArr(ans);
            }
            case 2: {
                String type = variables.get(1).get();

                switch (type) {
                    default:
                        return null;
                    case "ice":
                        return formatDoubleToStringArr(2.2918E8);
                }
            }
            case 3: {
                double ans = 0;

                return null;
            }
            case 4: {
                double ans = 0;

                return null;
            }
            case 5: {
                String type = variables.get(1).get();
                double angleR = rad(variables.get(2).getDouble());

                double index;
                switch (type) {
                    default:
                        return null;
                    case "flint glass":
                        index = 1.65;
                        break;
                }

                double ans = deg(Math.asin(index * Math.sin(angleR)));
                return formatDoubleToStringArr(ans);
            }
            case 6: {
                double ans = 1 / Math.sin(rad(variables.get(1).getDouble()));

                return formatDoubleToStringArr(ans);
            }
            case 7: {
                double angle1 = rad(variables.get(1).getDouble());
                double angle2 = rad(variables.get(2).getDouble());

                double ans = Math.sin(angle1) / Math.sin(angle2);

                return formatDoubleToStringArr(ans);
            }
            case 8: { //Constant answer
                return formatDoubleToStringArr(33.74);
            }
            case 9: { //TODO bonus
                double ans = 0;

                return null;
            }
            case 10: {
                double ans = variables.get(1).getDouble() * Math.tan(rad(48.61));

                return formatDoubleToStringArr(ans);
            }
            case 11: {
                double lengthT = variables.get(1).get();
                double angleR = rad(25.485);

                double lengthL = (1.45 - lengthT) * Math.tan(angleR);
                double ans = lengthL + lengthT;

                return formatDoubleToStringArr(ans);
            }
            case 12: {
                double ans1 = deg(Math.asin(1.662 * Math.sin(rad(variables.get(1).getDouble()))));
                double ans2 = deg(Math.asin(1.698 * Math.sin(rad(variables.get(1).getDouble()))));

                return formatDoubleToStringArr(ans1, ans2);
            }
            case 13: {
                double focal = variables.get(1).get();
                double d_o = variables.get(2).get();


                double ans = (d_o * focal) / (d_o - focal);
                return new String[]{format(ans), "virtual"};
            }
            case 14: {
                double d_o = variables.get(1).get();
                double focal = variables.get(2).get();

                double ans1 = (d_o * focal) / (d_o - focal);
                double ans2 = -ans1 / d_o;
                return formatDoubleToStringArr(ans1, ans2);
            }
            case 15: {
                double d_o = variables.get(1).get();

                double ans1 = ((0.035 * d_o) / (0.035 - d_o)) / d_o * 1.8;
                double ans2 = ((0.15 * d_o) / (0.15 - d_o)) / d_o * 1.8;
                return formatDoubleToStringArr(ans1, ans2);
            }
            case 16: {
                double focal = variables.get(1).getDouble() * pow(-3);
                double secondDist = variables.get(2).get();

                double dist1 = 1 / (1 / focal - 1 / 3.5);
                double dist2 = 1 / (1 / focal - 1 / secondDist);
                return formatDoubleToStringArr(dist1 - dist2);
            }
            case 17: {
                double d_o = variables.get(1).get();
                double nextFocal = variables.get(2).getDouble() * pow(-3);

                double d_i = 1 / (1 / .300 - 1 / d_o);
                double mag = d_i / d_o;
                double ans = nextFocal + nextFocal / mag;

                return formatDoubleToStringArr(ans);
            }
            case 18: {
                double dist = variables.get(1).get();

                double ans = (-8 * dist - 21.336) / (dist + 10.667);
                return formatDoubleToStringArr(ans);
            }
            case 19: {
                double focal = variables.get(1).getDouble();
                double d_o = variables.get(2).get();
                double distFromLens = variables.get(3).get();

                double d = 1 / (1 / focal - 1 / d_o);
                double ans = 1 / (1 / focal - 1 / (distFromLens - d));
                return new String[]{format(ans), ans > 0 ? "right" : "left"};
            }
            case 20: {
                double d_o = variables.get(1).getDouble();
                double mag = variables.get(2).get();

                double d_i = -1.7 * d_o;
                double f = 1 / (1 / d_o + 1 / (-mag * d_o));
                return new String[]{d_i < 0 ? "virtual" : "real", format(f), f > 0 ? "convex" : "concave"};
            }
        }

        return null;
    }
}
