package com.lsvirus.waft.core.solverimpls.hardcode;

import com.lsvirus.waft.core.Solver;
import com.lsvirus.waft.core.Variable;

import javax.measure.converter.UnitConverter;
import java.util.HashMap;

import static javax.measure.unit.SI.*;
import static javax.measure.unit.NonSI.*;


/**
 * Created by Muhatashim on 1/18/14.
 */
public class ThermodynamicsB extends Solver {
    @Override
    public String getName() {
        return "Thermodynamics (B)";
    }

    @Override
    public String[] solve(int questionNum, HashMap<Integer, Variable<?>> variables) {
        switch (questionNum) {
            case 1: {
                UnitConverter conv = CELSIUS.getConverterTo(KELVIN);

                double lengthO = variables.get(1).get();
                double naTemp = conv.convert(variables.get(2).getDouble());
                double seaTemp = conv.convert(variables.get(3).getDouble());

                double ans = lengthO * 12E-6 * (seaTemp - naTemp);
                return formatDoubleToStringArr(ans);
            }
            case 2: {
                UnitConverter conv = FAHRENHEIT.getConverterTo(KELVIN);

                double cold = conv.convert(variables.get(1).getDouble());
                double hot = conv.convert(variables.get(2).getDouble());

                double ans = 12E-6 * (hot - cold);
                return formatDoubleToStringArr(ans);
            }
            case 3: {
                double temp = variables.get(1).get();
                double gap = variables.get(2).getDouble() * pow(-3);

                double ans = (500000 * gap) / 31 + temp;
                return formatDoubleToStringArr(ans);
            }
            case 4: {
                double fireEndTemp = variables.get(1).get();
                double otherEndTemp = variables.get(2).get();
                double time = variables.get(3).get();

                double ans = 0.00520326 * time * (fireEndTemp - otherEndTemp);
                return formatDoubleToStringArr(ans);
            }
            case 5: {
                double sa = variables.get(1).get();
                double insthick = variables.get(2).get();

                double ans = (0.6 * sa) / insthick;
                return formatDoubleToStringArr(ans);
            }
            case 6: {
                double basmentWallTemp = variables.get(1).get();
                double insideSurfaceWallTemp = variables.get(2).get();
                double wallLength = variables.get(3).get();
                double wallArea = variables.get(4).get();

                double ans = 10000 / (1.1 * wallArea * (insideSurfaceWallTemp - basmentWallTemp) / wallLength);
                return formatDoubleToStringArr(ans);
            }
            case 7: {
                double volumeRoom = variables.get(1).get();
                double tempI = variables.get(2).get();
                double tempF = variables.get(3).get();

                double n1 = 1.01E5 * volumeRoom / (8.314 * tempI);
                double n2 = 1.01E5 * volumeRoom / (8.314 * tempF);

                double ans = (n1 - n2) * 28 / 1000;
                return formatDoubleToStringArr(ans);
            }
            case 8: {  //Constant ans
                return new String[]{"12196"};
            }
            case 9: {
                double velocity = variables.get(1).get();

                double ans = velocity / Math.sqrt(2);
                return formatDoubleToStringArr(ans);
            }
            case 10: {
                double gasPress = variables.get(3).getDouble() * pow(5);
                double aIV = variables.get(1).getDouble() * pow(-3);
                double aFV = variables.get(2).getDouble() * pow(-3);
                double bIV = variables.get(4).getDouble() * pow(-3);
                double bFV = variables.get(5).getDouble() * pow(-3);

                double ans = gasPress * (aIV - aFV);
                double ans2 = gasPress * (bIV - bFV);

                return formatDoubleToStringArr(ans, ans2);
            }
            case 11: {
                double mass = variables.get(1).get();
                double temperature = variables.get(2).get();

                double ans = 0.520927 * mass * temperature;
                return formatDoubleToStringArr(ans);
            }
            case 12: {
                double vol = variables.get(1).get();
                double heat = variables.get(2).getDouble() * pow(3);

                double ans1 = heat / (1.5 * 3 * 8.31);
                double ans2 = heat;
                double ans3 = 3 * 8.31 * ans1 / vol;
                return formatDoubleToStringArr(ans1, ans2, ans3);
            }
            case 13: {
                double final_ = variables.get(1).get();
                double initial = variables.get(2).get();

                double ans1 = 1.5 * 5 * 8.314 * (initial - final_);
                double ans2 = ans1;

                return formatDoubleToStringArr(ans1, ans2);
            }
            case 14: {
                double A = variables.get(1).get();
                double W = variables.get(2).get();

                double ans = (A - W) / (1.5 * .5 * 8.31);
                String ans2 = (ans > 0) ? "increase" : "decrease";

                return new String[]{format(ans), ans2};
            }
            case 15: {
                double F = variables.get(1).getDouble() * pow(-2);
                double W = variables.get(2).getDouble() * pow(3);

                double ans1 = 0;
                double ans2 = -W;
                double ans3 = (0.040093 * ans2) / (Math.log(18.182 * F));

                return formatDoubleToStringArr(ans1, ans2, ans3);
            }
            case 16: {
                double M = variables.get(1).get();
                double H = variables.get(2).get();
                double X = variables.get(3).get();

                double ans = (100 * (H - X)) / (981 * M);
                return formatDoubleToStringArr(ans);
            }
            case 17: {
                double N = variables.get(1).get();
                double T = variables.get(2).get();
                double X = variables.get(3).get();

                double ans = N * 8.314 * T * Math.log(1 / X);
                return formatDoubleToStringArr(ans);
            }
            case 18: {
                UnitConverter litConv = LITER.getConverterTo(CUBIC_METRE);

                double vc = litConv.convert(variables.get(2).getDouble());

                double ans1 = -141855 * (vc - 0.0068);
                double ans2 = 0;
                double ans3 = -ans1;
                return formatDoubleToStringArr(ans1, ans2, ans3);
            }
            case 19: {
                return new String[]{"-2700", "4050", "4050"};
            }
            case 20: {
                double E = variables.get(1).getDouble() / 100;
                double W = variables.get(2).get();

                double ans1 = W / E;
                double ans2 = ans1 - W;

                return formatDoubleToStringArr(ans1, ans2);
            }
            case 21: {
                double H = variables.get(1).get();
                double C = variables.get(2).get();

                double ans = (1 - C / H) * 0.6;
                return formatDoubleToStringArr(ans);
            }
            case 22: {
                double H = variables.get(1).get();
                double C = variables.get(2).get();

                double ans = (1 - C / H) * 5000;
                return formatDoubleToStringArr(ans);
            }
            case 23: {
                double R = variables.get(1).get();
                double D = variables.get(2).get();

                double ans1 = D - R;
                double ans2 = D / ans1;
                return formatDoubleToStringArr(ans1, ans2);
            }
            case 24: {
                //Too hard, m8...
            }
        }
        return null;
    }
}
