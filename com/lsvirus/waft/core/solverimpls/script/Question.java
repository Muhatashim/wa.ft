package com.lsvirus.waft.core.solverimpls.script;

import java.util.Arrays;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 3/17/14
 * Time: 3:54 PM
 */
public class Question {

    private int                  questionNumber;
    private Map<String, Integer> variableMapping;
    private String[]             answers;

    public int getQuestionNumber() {
        return questionNumber;
    }

    public Map<String, Integer> getVariableMapping() {
        return variableMapping;
    }

    public int forVariable(String name) {
        return variableMapping.get(name);
    }

    public String forIndex(int index) {
        for (Map.Entry<String, Integer> entry : variableMapping.entrySet())
            if (entry.getValue() == index)
                return entry.getKey();

        return null;
    }

    public String[] getAnswerAlgos() {
        return answers;
    }

    @Override
    public String toString() {
        return "Question{" +
                "questionNumber=" + questionNumber +
                ", variableMapping=" + variableMapping +
                ", answers=" + Arrays.toString(answers) +
                '}';
    }
}
