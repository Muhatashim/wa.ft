package com.lsvirus.waft.core.solverimpls.script;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 3/17/14
 * Time: 3:53 PM
 */
public class Assignment {

    private String assignment;

    private Question[] questions;

    public String getAssignmentName() {
        return assignment;
    }

    public Question[] getQuestions() {
        return questions;
    }

    public Question getQuestion(int num) {
        for (Question question: questions)
            if (question.getQuestionNumber() == num)
                return question;

        return null;
    }

    @Override
    public String toString() {
        return "Assignment{" +
                "assignment='" + assignment + '\'' +
                ", questions=" + Arrays.toString(questions) +
                '}';
    }
}
