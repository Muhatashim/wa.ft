package com.lsvirus.waft.core.solverimpls.script;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lsvirus.waft.core.Solver;
import com.lsvirus.waft.core.Variable;
import com.showyourwork.engine.Equation;
import com.showyourwork.engine.EquationBuilder;
import com.showyourwork.engine.EquationParser;
import org.jscience.mathematics.number.Rational;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 3/17/14
 * Time: 3:33 PM
 */
public class Script extends Solver {

    private Assignment assignment;

    public Script(File fileLoc) throws FileNotFoundException {
        Gson gson = new GsonBuilder().create();

        assignment = gson.fromJson(new FileReader(fileLoc), Assignment.class);
    }

    @Override
    public String getName() {
        return assignment.getAssignmentName();
    }

    @Override
    public String[] solve(int questionNum, HashMap<Integer, Variable<?>> variables) throws FileNotFoundException, ScriptException {
        Question question = assignment.getQuestion(questionNum);

        if (question == null)
            return null;

        ScriptEngine jruby = new ScriptEngineManager().getEngineByName("jruby");

        FileReader fileReader = new FileReader("./backbone.rb");
        jruby.eval(fileReader);

        if (question.getVariableMapping() != null)
            for (Map.Entry<String, Integer> variableMap : question.getVariableMapping().entrySet())
                jruby.eval("$" + variableMap.getKey() + "=" + variables.get(variableMap.getValue()).get());

        String[] answerAlgos = question.getAnswerAlgos();
        String[] answers = new String[answerAlgos.length];

        for (int i = 0; i < answers.length; i++) {
            answers[i] = jruby.eval(answerAlgos[i]).toString();
        }

        return answers;
    }

    public static long solve(String eq, int whichAnswer) {
        EquationBuilder b = new EquationBuilder();
        new EquationParser(b).parse(eq);

        Equation e = b.build();
        e.evaluateUsingNewtonsMethod();

        Set<Rational> answers = e.getAnswers();
        return answers.toArray(new Rational[answers.size()])[whichAnswer].longValue();
    }
}
