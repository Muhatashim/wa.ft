package com.lsvirus.waft;

import com.lsvirus.waft.core.AssignmentProcessor;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.swing.*;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 1/17/14
 * Time: 5:35 PM
 */
public class ShowAssignments {

    private Scene scene;

    public ShowAssignments(String user, String institution) throws IOException {
        VBox vBox = new VBox(5);
        scene = new Scene(vBox);


        vBox.getChildren().add(new Text("Select an assignment to complete"));

        Document document = Main.goTo(String.format("http://www.webassign.net/v4cgi%s@%s/student.pl", user, institution));
        Elements homework = document.select("[class=crop content container] [class=row container]");

        for (final Element element : homework) {
            final Elements a = element.select("a[onclick]");

            if (!a.isEmpty()) {
                final String assignmentName = a.text();
                final Button button = new Button(assignmentName);

                button.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent actionEvent) {
                        String onclick = a.attr("onclick");

                        try {
                            new AssignmentProcessor(
                                    onclick.substring(onclick.indexOf(' ') + 1, onclick.length() - 16),
                                    assignmentName).run();
                        } catch (Exception e) {
                            System.out.println(assignmentName);
                            JOptionPane.showMessageDialog(null, e);
                            e.printStackTrace();
                        }
                    }
                });

                button.setMaxWidth(150);
                vBox.getChildren().add(button);
                VBox.setVgrow(button, Priority.ALWAYS);
            }
        }

        vBox.setPadding(new Insets(10, 10, 10, 10));
        vBox.setAlignment(Pos.CENTER);
    }

    public Scene getScene() {
        return scene;
    }
}
